import { createSlice } from "@reduxjs/toolkit";

const initialState = [
    { id: 1, name: "Kongden", email: "Kongden1@gmail.com" },
    { id: 2, name: "Jonh", email: "Jonh@gmail.com" }
];


const userSlice = createSlice({
    name:'user',
    initialState,
    reducers:{

        addUser: (state,action) => {
            state.push(action.payload)
        },

        updateUser: (state,action) => {
            //destrcuturing the payload we got from input 
            const {id,name,email} = action.payload;

            //filtering initial state and checking if there is an equal id that destructure from the 
            //payload and the id that being filtered from the state
            const existingUser = state.find(user => user.id == id)
            if(existingUser){
                existingUser.name = name;
                existingUser.email = email;
            }
        },

        deleteUser: (state,action) => {
            const {id} = action.payload

            const existingUser = state.find(user => user.id == id);
            if(existingUser){
                return state.filter(user => user.id !== id)
            }
        }
    }
});

export const {addUser,updateUser,deleteUser} = userSlice.actions
export default userSlice.reducer;