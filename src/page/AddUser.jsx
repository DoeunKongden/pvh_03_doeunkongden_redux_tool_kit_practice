import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { addUser } from '../app/features/userSlice'
import Botton from '../components/Botton'
import TextField from '../components/TextField'
import { v4 as uuidv4 } from 'uuid';

function AddUser() {

    const dispatch = useDispatch();

    const navigate = useNavigate();

    const [values, setValues] = useState({
        name: '',
        email: ''
    })

    const handleAddUser = () => {
        setValues({ name: '', email: '' });

        dispatch(addUser({
            id: uuidv4(),
            name: values.name,
            email: values.email
        }))
        
        navigate('/')
    }

    return (
        <div className='mt-10 max-w-xl mx-auto'>
            <TextField
                label="Name"
                value={values.name}
                onChange={(e) => setValues({ ...values, name: e.target.value })}
                inputProps={{ type: 'text', placeholder: 'Kongden' }}
            />
            <br />
            <TextField
                label="Email"
                value={values.email}
                onChange={(e) => setValues({ ...values, email: e.target.value })}
                inputProps={{ type: 'email', placeholder: 'Kongden@gmail.com' }}
            />

            <Botton onClick={handleAddUser}>Submit</Botton>
        </div>
    )
}

export default AddUser