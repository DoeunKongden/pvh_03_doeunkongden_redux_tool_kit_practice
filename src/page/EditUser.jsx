import React, { useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import Botton from '../components/Botton'
import TextField from '../components/TextField'
import { useDispatch, useSelector } from 'react-redux'
import { updateUser } from '../app/features/userSlice'


function EditUser() {

    const params = useParams();

    const navigate = useNavigate();

    const users = useSelector(store => store.users)

    // console.log(users);

    const existingUser = users.filter((user) => user.id == params.id);

    // console.log(existingUser[0].email);

    const { name, email } = existingUser[0];
    const dispatch = useDispatch();
    const [values, setValues] = useState({
        name: name,
        email: email
    })

    const handleUpdateUser = () => {
        setValues({ name: '', email: '' });
        dispatch(updateUser({
            id: params.id,
            name: values.name,
            email: values.email
        }))
        navigate('/')
    }

    return (
        <div className='mt-10 max-w-xl mx-auto'>
            <TextField
                label="Name"
                value={values.name}
                onChange={(e) => setValues({ ...values, name: e.target.value })}
                inputProps={{ type: 'text', placeholder: 'Kongden' }}
            />
            <br />
            <TextField
                label="Email"
                value={values.email}
                onChange={(e) => setValues({ ...values, email: e.target.value })}
                inputProps={{ type: 'email', placeholder: 'Kongden@gmail.com' }}
            />

            <Botton onClick={handleUpdateUser}>Update User</Botton>
        </div>
    )
}

export default EditUser