import { Route, Routes } from "react-router-dom";
import UserList from "./page/UserList";
import AddUser from "./page/AddUser";
import EditUser from "./page/EditUser";


function App() {
  return (
    <div className="container mx-auto px-2 max-w-5xl pt-10 md:pt-32">
      <h1 className="text-center font-bold text-2xl text-gray-700 mb-10">CRUD WITH REDUX TOOL KIT</h1>

      <Routes>
        <Route path="/" element={<UserList />} />
        <Route path="/add-user" element={<AddUser />}/>
        <Route path="/edit-user/:id" element={<EditUser />}/>
      </Routes>
    </div>
  );
}

export default App;
